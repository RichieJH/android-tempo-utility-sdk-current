package com.bluemaestro.tempo_utility.views.generic;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Garrett on 05/08/2016.
 */
public class BMButton extends Button {

    public BMButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
        super.setTypeface(font);
    }
}
