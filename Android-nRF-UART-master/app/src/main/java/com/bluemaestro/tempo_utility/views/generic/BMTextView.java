package com.bluemaestro.tempo_utility.views.generic;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Garrett on 05/08/2016.
 */
public class BMTextView extends TextView {

    public BMTextView(Context context){
        super(context);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
        super.setTextColor(Color.BLACK);
        super.setTypeface(font);
    }

    public BMTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
        super.setTextColor(Color.BLACK);
        super.setTypeface(font);
    }
}
