package com.bluemaestro.tempo_utility;

/**
 * Created by Garrett on 12/09/2016.
 */
public final class Log {

    private static final boolean LOG = BuildConfig.DEBUG;

    private Log(){

    }

    /**
     * Info
     * @param tag
     * @param message
     */
    public static final void i(String tag, String message){
        if(LOG) android.util.Log.i(tag, message);
    }

    /**
     * Warn
     * @param tag
     * @param message
     */
    public static final void w(String tag, String message){
        if(LOG) android.util.Log.w(tag, message);
    }

    /**
     * Exception
     * @param tag
     * @param message
     */
    public static final void e(String tag, String message){
        if(LOG) android.util.Log.e(tag, message);
    }

    /**
     * Debug
     * @param tag
     * @param message
     */
    public static final void d(String tag, String message){
        if(LOG) android.util.Log.d(tag, message);
    }

    /**
     * Verbose
     * @param tag
     * @param message
     */
    public static final void v(String tag, String message){
        if(LOG) android.util.Log.v(tag, message);
    }
}
