package com.bluemaestro.tempo_utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.sql.BMPebbleDatabase27;
import com.bluemaestro.tempo_utility.sql.BMTempHumiDatabase23;

import java.util.ArrayList;

/**
 * Created by iMac27 on 13/03/2017.
 */

public class CustomAdapter23 extends BaseAdapter {

    private Context context;
    private ArrayList<BMTempHumiDatabase23.Readings> readings23;
    private LayoutInflater inflater;
    private String unitsForTemperature;
    private String unitsForHumidity;

    public CustomAdapter23(Context context, ArrayList<BMTempHumiDatabase23.Readings>readings, String units) {
        this.context = context;
        this.readings23 = readings;
        this.unitsForTemperature = units;
        this.unitsForHumidity = "% RH";

    }

    @Override
    public int getCount() {
        return readings23.size();
    }

    @Override
    public Object getItem(int position) {
        return readings23.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            view = inflater.inflate(R.layout.table_element, parent, false);
        }

        TextView logNumber = (TextView)view.findViewById(R.id.log_number);
        TextView timeStamp = (TextView)view.findViewById(R.id.log_timestamp);
        TextView temperatureValue = (TextView)view.findViewById(R.id.log_temperature_value);
        TextView humidityValue = (TextView)view.findViewById(R.id.log_humidity_value);
        TextView dewpointValue = (TextView)view.findViewById(R.id.log_dewpoint_value_23);
        TextView divider = (TextView)view.findViewById(R.id.dividerfor23);

        logNumber.setVisibility(View.VISIBLE);
        timeStamp.setVisibility(View.VISIBLE);
        temperatureValue.setVisibility(View.VISIBLE);
        humidityValue.setVisibility(View.VISIBLE);
        dewpointValue.setVisibility(View.VISIBLE);
        divider.setVisibility(View.VISIBLE);


        logNumber.setText("Log number : "+String.valueOf(readings23.get(position).getIndexValue()));
        timeStamp.setText(readings23.get(position).getDateStamp());
        if (unitsForTemperature.matches("º F")) {
            Float temperatureValueForDisplayNum = (float)((readings23.get(position).getTemparatureValue() * 1.8) + 32);
            String temperatureValueForDisplayString = String.format("%.1f", temperatureValueForDisplayNum);
            temperatureValue.setText(temperatureValueForDisplayString+" "+unitsForTemperature);

            Float dewpointValueForDisplayNum = (float)((readings23.get(position).getDewpointValue() * 1.8) + 32);
            String dewpointValueForDisplayString = String.format("%.1f", dewpointValueForDisplayNum);
            dewpointValue.setText(dewpointValueForDisplayString+" "+unitsForTemperature);
        } else {
            temperatureValue.setText(String.valueOf(readings23.get(position).getTemparatureValue()) + " " + unitsForTemperature);
            dewpointValue.setText(String.valueOf(readings23.get(position).getDewpointValue()) + " " + unitsForTemperature);
        }

        humidityValue.setText(String.valueOf(readings23.get(position).getHumidityValue()) + " " + unitsForHumidity);
        return view;

    }




}