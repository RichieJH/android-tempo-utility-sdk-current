package com.bluemaestro.tempo_utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.sql.BMTempDatabase13;

import java.util.ArrayList;

/**
 * Created by iMac27 on 13/03/2017.
 */

public class CustomAdapter13 extends BaseAdapter {

    private Context context;
    private ArrayList<BMTempDatabase13.Readings> readings13;
    private LayoutInflater inflater;
    private String unitsForTemperature;

    public CustomAdapter13(Context context, ArrayList<BMTempDatabase13.Readings>readings, String units) {
        this.context = context;
        this.readings13 = readings;
        this.unitsForTemperature = units;

    }

    @Override
    public int getCount() {
        return readings13.size();
    }

    @Override
    public Object getItem(int position) {
        return readings13.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            view = inflater.inflate(R.layout.table_element, parent, false);
        }

        TextView logNumber = (TextView)view.findViewById(R.id.log_number);
        TextView timeStamp = (TextView)view.findViewById(R.id.log_timestamp);
        TextView temperatureValue = (TextView)view.findViewById(R.id.log_temperature_value);
        TextView divider = (TextView)view.findViewById(R.id.dividerfor13);

        logNumber.setVisibility(View.VISIBLE);
        timeStamp.setVisibility(View.VISIBLE);
        temperatureValue.setVisibility(View.VISIBLE);
        divider.setVisibility(View.VISIBLE);

        logNumber.setText("Log number : "+String.valueOf(readings13.get(position).getIndexValue()));
        timeStamp.setText(readings13.get(position).getDateStamp());
        if (unitsForTemperature.matches("º F")) {
            Float temperatureValueForDisplayNum = (float)((readings13.get(position).getTemparatureValue() * 1.8) + 32);
            String temperatureValueForDisplayString = String.format("%.1f", temperatureValueForDisplayNum);
            temperatureValue.setText(temperatureValueForDisplayString+" "+unitsForTemperature);

        } else {
            temperatureValue.setText(String.valueOf(readings13.get(position).getTemparatureValue()) + " " + unitsForTemperature);
        }

        return view;

    }




}