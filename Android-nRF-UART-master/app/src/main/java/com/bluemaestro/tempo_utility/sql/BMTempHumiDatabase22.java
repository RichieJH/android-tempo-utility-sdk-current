package com.bluemaestro.tempo_utility.sql;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Color;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;
import com.github.mikephil.charting.charts.Chart;
import com.bluemaestro.tempo_utility.UartService;
import com.bluemaestro.tempo_utility.devices.BMTempHumi22;
import com.bluemaestro.tempo_utility.devices.BMTempHumi23;
import com.bluemaestro.tempo_utility.sql.downloading.BMDownloader;
import com.bluemaestro.tempo_utility.sql.downloading.DownloadState;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Garrett on 23/08/2016.
 */
public class BMTempHumiDatabase22 extends BMDatabase {

    // Keys
    private static final String KEY_TEMP = "temperature";
    private static final String KEY_HUMI = "humidity";
    private static final String KEY_DEWP = "dew_point";

    // Log commands
    // *lint2 = log at 2 seconds
    //private static final String LOG_TEMP = "*logntemp";
    //private static final String LOG_HUMI = "*lognhumi";
    //private static final String LOG_DEWP = "*logndewp";

    private BMTempHumi22 mBMTempHumi22;
    private BMTempHumi23 mBMTempHumi23;


    public BMTempHumiDatabase22(Context context, BMTempHumi22 bmTempHumi) {
        super(context, bmTempHumi);
        CREATE_TABLE_DATA += "," + KEY_TEMP + " REAL";
        CREATE_TABLE_DATA += "," + KEY_HUMI + " REAL";
        CREATE_TABLE_DATA += "," + KEY_DEWP + " REAL";
        this.mBMTempHumi22 = bmTempHumi;
    }


    public BMTempHumiDatabase22(Context context, String address) {
        super(context, address);
        CREATE_TABLE_DATA += "," + KEY_TEMP + " REAL";
        CREATE_TABLE_DATA += "," + KEY_HUMI + " REAL";
        CREATE_TABLE_DATA += "," + KEY_DEWP + " REAL";
    }

    @Override
    public ContentValues addToValues(ContentValues values, String key, double data) {
        if(key.equals(KEY_TEMP) || key.equals(KEY_DEWP) || key.equals(KEY_HUMI)){
            values.put(key, data / 10.0);
        }
        return values;
    }

    @Override
    public DownloadState downloadData(UartService service, BMDownloader downloader, BMProgressIndicator progress) throws UnsupportedEncodingException {
        return downloadData(service, downloader,
                new String[]{
                        KEY_TEMP,
                        KEY_HUMI,
                        KEY_DEWP
                },
                true, "none", progress);
    }

    @Override
    public void displayAsInfo(ArrayAdapter<String> listAdapter){
        super.displayAsInfo(listAdapter);
        /*SQLiteDatabase db = getReadableDatabase();

        // Temperature
        String tempHigh = DatabaseUtils.stringForQuery(db,
                "SELECT MAX(" + KEY_TEMP + ") FROM " + TABLE_DATA, null);
        String tempLow = DatabaseUtils.stringForQuery(db,
                "SELECT MIN(" + KEY_TEMP + ") FROM " + TABLE_DATA, null);
        String tempAvg = DatabaseUtils.stringForQuery(db,
                "SELECT AVG(" + KEY_TEMP + ") FROM " + TABLE_DATA, null);

        if(isInFahrenheit()){
            tempHigh = Utility.convertValueTo(tempHigh, getTempUnits());
            tempLow = Utility.convertValueTo(tempLow, getTempUnits());
            tempAvg = Utility.convertValueTo(tempAvg, getTempUnits());
        }

        listAdapter.add("Highest temperature: " + tempHigh + getTempUnits());
        listAdapter.add("Lowest temperature: " + tempLow + getTempUnits());
        listAdapter.add("Average temperature: " + tempAvg + getTempUnits());
        listAdapter.notifyDataSetChanged();*/
    }

    @Override
    public void displayAsChart(Chart chart, BMProgressIndicator progress) {
        if (!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        displayAsChart(
                getVersion(),
                lineChart,
                new String[]{
                        KEY_TEMP,
                        KEY_HUMI,
                        KEY_DEWP
                },
                new String[]{
                        KEY_TEMP,
                        KEY_DEWP
                },
                new int[]{
                        Color.RED,
                        Color.BLUE,
                        Color.GREEN
                },
                progress
        );
    }

    @Override
    public void displayAsTable(Context context, ListView listView) {
        displayAsTable(context, listView, (int)mBMTempHumi22.getVersion(), mBMTempHumi22.getTempUnits()
        );
    }

    @Override
    public File export(Context context, String filename) throws IOException {
        return export(context, filename, (int)mBMTempHumi22.getVersion(), mBMTempHumi22.getTempUnits());
    }
}
