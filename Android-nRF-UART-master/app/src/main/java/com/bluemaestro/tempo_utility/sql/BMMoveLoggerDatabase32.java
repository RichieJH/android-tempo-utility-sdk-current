package com.bluemaestro.tempo_utility.sql;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Color;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bluemaestro.tempo_utility.UartService;
import com.bluemaestro.tempo_utility.devices.BMMoveLogger32;
import com.bluemaestro.tempo_utility.sql.downloading.BMDownloader;
import com.bluemaestro.tempo_utility.sql.downloading.DownloadState;
import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;
import com.github.mikephil.charting.charts.Chart;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Garrett on 23/08/2016.
 */
public class BMMoveLoggerDatabase32 extends BMDatabase {


    // Keys
    private static final String KEY_CHANNEL_ONE = "channel_one";
    private static final String KEY_CHANNEL_TWO = "channel_two";
    private static final String KEY_TIME = "time";
    private static final String LAST_DOWNLOAD_DATE = "last_download_date";

    private BMMoveLogger32 mBMMoveLogger32;

    public BMMoveLoggerDatabase32(Context context, BMMoveLogger32 bmMoveLogger32) {
        super(context, bmMoveLogger32);
        CREATE_TABLE_DATA += "," + KEY_CHANNEL_ONE + " REAL";
        CREATE_TABLE_DATA += "," + KEY_CHANNEL_TWO + " REAL";
        CREATE_TABLE_DATA += "," + KEY_TIME + " TIMESTAMP";
        this.mBMMoveLogger32 = bmMoveLogger32;

    }

    public static class Readings {
        int indexValue;
        int channelOneValue;
        int channelTwoValue;
        String dateStamp;

        public int getIndexValue() {
            return indexValue;
        }

        public void setIndexValue(int value) {
            this.indexValue = value;
        }

        public float getChannelOneValue() {
            return channelOneValue;
        }

        public void setChannelOneValue(int value) {
            this.channelOneValue = value;
        }

        public float getChannelTwoValue() {
            return channelTwoValue;
        }

        public void setChannelTwoValue(int value) {
            this.channelTwoValue = value;
        }

        public String getDateStamp() {
            return dateStamp;
        }

        public void setDateStamp(String value) {
            this.dateStamp = value;
        }
    }


    @Override
    public ContentValues addToValues(ContentValues values, String key, double data) {
        if(key.equals(KEY_CHANNEL_ONE) || key.equals(KEY_CHANNEL_TWO)){
            values.put(key, data);
        }
        return values;
    }

    @Override
    public DownloadState downloadData(UartService service, BMDownloader downloader, BMProgressIndicator progress) throws UnsupportedEncodingException {
        return downloadData(service, downloader,
                new String[]{
                        KEY_CHANNEL_ONE,
                        KEY_CHANNEL_TWO
                },
                true, mBMMoveLogger32.getReferenceDate(), progress);
    }

    @Override
    public void displayAsInfo(ArrayAdapter<String> listAdapter){
        super.displayAsInfo(listAdapter);
    }

    @Override
    public void displayAsChart(Chart chart, BMProgressIndicator progress) {
        if (!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        displayAsChart(
                getVersion(),
                lineChart,
                new String[]{
                        KEY_CHANNEL_ONE,
                        KEY_CHANNEL_TWO
                },
                new String[]{
                        KEY_CHANNEL_ONE,
                        KEY_CHANNEL_TWO
                },
                new int[]{
                        Color.RED,
                        Color.BLUE,
                },
                progress
        );
    }

    @Override
    public void displayAsTable(Context context, ListView listView) {
        displayAsTable(context, listView, (int) mBMMoveLogger32.getVersion(), mBMMoveLogger32.getReferenceDate());
    }

    @Override
    public File export(Context context, String filename) throws IOException {
        return export(context, filename, (int) mBMMoveLogger32.getVersion(), mBMMoveLogger32.getReferenceDate());
    }
}