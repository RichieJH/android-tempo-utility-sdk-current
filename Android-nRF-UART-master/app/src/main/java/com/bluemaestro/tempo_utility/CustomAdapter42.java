package com.bluemaestro.tempo_utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.sql.BMButtonDatabase42;

import java.util.ArrayList;

/**
 * Created by iMac27 on 13/03/2017.
 */

public class CustomAdapter42 extends BaseAdapter {

    private Context context;
    private ArrayList<BMButtonDatabase42.Readings> readings42;
    private LayoutInflater inflater;
    private String unitsForPushes;

    public CustomAdapter42(Context context, ArrayList<BMButtonDatabase42.Readings>readings, String units) {
        this.context = context;
        this.readings42 = readings;
        this.unitsForPushes = units;

    }

    @Override
    public int getCount() {
        return readings42.size();
    }

    @Override
    public Object getItem(int position) {
        return readings42.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            view = inflater.inflate(R.layout.table_element, parent, false);
        }
        TextView logNumber = (TextView)view.findViewById(R.id.log_number);
        TextView timeStamp = (TextView)view.findViewById(R.id.log_timestamp);
        TextView buttonValue = (TextView)view.findViewById(R.id.log_button_value);
        TextView buttonLabel = (TextView)view.findViewById(R.id.log_button_label);
        TextView divider = (TextView)view.findViewById(R.id.dividerfor42);

        logNumber.setVisibility(View.VISIBLE);
        timeStamp.setVisibility(View.VISIBLE);
        buttonValue.setVisibility(View.VISIBLE);
        buttonLabel.setVisibility(View.VISIBLE);
        divider.setVisibility(View.VISIBLE);

        logNumber.setText("Log number : "+String.valueOf(readings42.get(position).getIndexValue()));
        timeStamp.setText(readings42.get(position).getDateStamp());
        float floatCount = readings42.get(position).getButtonCountValue();
        int count = (int)floatCount;
        buttonValue.setText(count + " Pushes");
        return view;
    }
}