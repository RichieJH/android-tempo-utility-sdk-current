package com.bluemaestro.tempo_utility.ble;

import com.bluemaestro.tempo_utility.UartService;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Garrett on 24/08/2016.
 */
public final class Utility {

    String referenceDateString;

    public Utility(){

    }

    public static final String bytesAsHexString(byte[] bytes){
        StringBuilder stringBuilder = new StringBuilder(bytes.length);
        for(byte byteChar : bytes)
            stringBuilder.append(String.format("%02X", byteChar)).append(" ");
        return stringBuilder.toString();
    }

    public static final String convertValueTo(String value, String units){
        if(value == null) return null;
        float val = new Float(value);
        switch(units.charAt(units.length() - 1)){
            case 'F':
                val = val * 1.8f + 32;
                break;
            default:
                break;
        }
        return String.format("%.1f", val);
    }

    public static final String formatKey(String key){
        key = key.trim();
        key = key.replace("_", " ");
        key = key.toLowerCase();
        char[] array = key.toCharArray();
        for(int i = 0; i < array.length - 1; i++){
            if(array[i] == ' ') array[i+1] = Character.toUpperCase(array[i+1]);
        }
            array[0] = Character.toUpperCase(array[0]);
            return new String(array);
        }

    public static final void sendCommand(UartService service, String command) throws UnsupportedEncodingException {
        service.writeRXCharacteristic(command.getBytes("UTF-8"));
    }

    public String convertNumberIntoDate (String referenceDateNumber) {
        referenceDateString = "null";
        int numberPassedIn = Integer.parseInt(referenceDateNumber);
        SimpleDateFormat shortFormat = new SimpleDateFormat("yy/MM/dd HH:mm");
        SimpleDateFormat longFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");

        if (referenceDateNumber.length()>10 || numberPassedIn == 0) {
            return referenceDateString;
        }

        int minutes = numberPassedIn % 100;
        int hours = (numberPassedIn/100) % 100;
        int days = (numberPassedIn/10000) % 100;
        int months = (numberPassedIn/1000000) % 100;
        int years = (numberPassedIn/100000000) % 100;

        String referenceDate = String.valueOf(years) +"/"+String.valueOf(months)+"/"+String.valueOf(days)+" "+String.valueOf(hours)+":"+String.valueOf(minutes);

        try {
            Date referenceDateDate = shortFormat.parse(referenceDate);
            referenceDateString = longFormat.format(referenceDateDate);
            //Log.d("Utility", "Date number passed in is "+referenceDateNumber+" date parsed is " + referenceDateString);
            /*  //Code that increments time by 3600 seconds (or 1 hour)
            Calendar cal = Calendar.getInstance();
            cal.setTime(referenceDateDate);
            cal.add(Calendar.SECOND, 3600);
            Date newDate = cal.getTime();
            */

        } catch (ParseException e) {
            //Log.d("Utility", "Date parsing failed");
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return referenceDateString;
    }

}
