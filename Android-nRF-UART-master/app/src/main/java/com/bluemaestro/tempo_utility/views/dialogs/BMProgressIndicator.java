package com.bluemaestro.tempo_utility.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.widget.Button;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.StyleOverride;
import com.bluemaestro.tempo_utility.views.generic.BMTextView;

/**
 * Created by iMac27 on 02/09/2017.
 */

public class BMProgressIndicator {

    private ProgressDialog progressDialog;
    private int max;
    Context context;


    public BMProgressIndicator(Context context, String title, String message){
        this.context = context;
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setTitle("Tempo Utility");
        this.progressDialog.setMessage(message);
        this.progressDialog.setIndeterminate(false);
        this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    public void setNegativeButton(){
        this.progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressDialog.dismiss();
            }
        });
    }
    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener){
        this.progressDialog.setOnCancelListener(onCancelListener);
    }

    public void showProgressDialog(){

        this.progressDialog.show();
    }

    public void setProgress(int progress){
        this.progressDialog.setProgress(progress);
        if (progress+1 >= max) progressDialog.dismiss();
    }

    public void setMax(int max){
        this.max = max*2;
        this.progressDialog.setMax(this.max);
    }

    public void dismiss(){
        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void setProgressReferenceDate(int progress){
        int progressAdjusted = this.max/2 + progress;
        this.progressDialog.setProgress(progressAdjusted);
        if (progressAdjusted+2 >= max) progressDialog.dismiss();
    }


    public void applyFont(Context context, String typeface){
        if(progressDialog == null) return;
        String fontPath = "fonts/Montserrat-Regular.ttf";
        Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);
        ((TextView) progressDialog.findViewById(context.getResources().getIdentifier(
                "alertTitle", "id", "android"))).setTypeface(tf);

        TextView title = ((TextView) progressDialog.findViewById(context.getResources().getIdentifier(
                "alertTitle", "id", "android")));
        title.setTextSize(18);
        title.setTextColor(Color.BLACK);
        TextView message = (TextView) progressDialog.findViewById(android.R.id.message);
        Button yes = progressDialog.getButton(Dialog.BUTTON_POSITIVE);
        Button no = progressDialog.getButton(Dialog.BUTTON_NEGATIVE);

        StyleOverride.setDefaultFont(message, context, typeface);
        message.setTextSize(15);
        if(yes != null){
            StyleOverride.setDefaultFont(yes, context, typeface);
            yes.setTextSize(17);
        }
        if(no != null) {
            StyleOverride.setDefaultFont(no, context, typeface);
            no.setTextSize(17);
        }

    }


    public void preventCancelOnClickOutside() {
        if (progressDialog != null) {
            progressDialog.setCanceledOnTouchOutside(false);
        }
    }


}
