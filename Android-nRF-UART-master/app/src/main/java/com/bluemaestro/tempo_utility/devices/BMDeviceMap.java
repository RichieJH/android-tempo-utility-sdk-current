package com.bluemaestro.tempo_utility.devices;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import com.bluemaestro.tempo_utility.Log;

import com.bluemaestro.tempo_utility.sql.BMButtonDatabase42;
import com.bluemaestro.tempo_utility.sql.BMDatabase;
import com.bluemaestro.tempo_utility.sql.BMDefaultDatabase;
import com.bluemaestro.tempo_utility.sql.BMMoveLoggerDatabase32;
import com.bluemaestro.tempo_utility.sql.BMPebbleDatabase27;
import com.bluemaestro.tempo_utility.sql.BMTempDatabase13;
import com.bluemaestro.tempo_utility.sql.BMTempHumiDatabase22;
import com.bluemaestro.tempo_utility.sql.BMTempHumiDatabase23;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Garrett on 15/08/2016.
 *
 * This is a map which will create a Blue Maestro device, given its ID.
 * This allows less duplication of code, and contains the Blue Maestro device
 * creation in one area
 */
public enum BMDeviceMap {

    INSTANCE;

    /**
     * Map of ID to Blue Maestro device
     */
    private final Map<Byte, Class<? extends BMDevice>> dvMap;
    private final Map<String, BMDevice> deviceMap;
    private final Map<Byte, Class<? extends BMDatabase>> dbMap;
    private final Map<String, BMDatabase> databaseMap;

    /**
     * IDs
     */
    private static final byte TEMPO_DISC_T          = 1;
    private static final byte TEMPO_TEMP            = 13;
    private static final byte TEMPO_TEMP_BEACON     = 113;
    private static final byte TEMPO_TEMPHUMI22      = 22;
    private static final byte TEMPO_TEMPHUMI23      = 23;
    private static final byte PEBBLE27              = 27;
    private static final byte BAIT_SAFE             = 33;
    private static final byte MOVE_LOGGER           = 32;
    private static final byte BM_BUTTON             = 42;

    /**
     * Constructor
     */
    BMDeviceMap(){
        this.dvMap = new HashMap<Byte, Class<? extends BMDevice>>();
        this.deviceMap = new HashMap<String, BMDevice>();
        this.dbMap = new HashMap<Byte, Class<? extends BMDatabase>>();
        this.databaseMap = new HashMap<String, BMDatabase>();

        dvMap.clear();
        deviceMap.clear();
        dbMap.clear();
        databaseMap.clear();

        initializeIDs();
    }

    /**
     * Initialize IDs to classes
     */
    public final void initializeIDs(){
        // Blue Maestro devices
        dvMap.put(TEMPO_DISC_T,             BMTempoDiscT.class);
        dvMap.put(TEMPO_TEMP,               BMTemp13.class);
        dvMap.put(TEMPO_TEMP_BEACON,        BMTemp13.class);
        dvMap.put(TEMPO_TEMPHUMI22,         BMTempHumi22.class);
        dvMap.put(TEMPO_TEMPHUMI23,         BMTempHumi23.class);
        dvMap.put(PEBBLE27,                 BMPebble27.class);
        dvMap.put(BAIT_SAFE,                BMBaitsafe.class);
        dvMap.put(MOVE_LOGGER,              BMMoveLogger32.class);
        dvMap.put(BM_BUTTON,                BMButton42.class);

        // Blue Maestro databases
        dbMap.put(TEMPO_TEMP,              BMTempDatabase13.class);
        dbMap.put(TEMPO_TEMP_BEACON,       BMTempDatabase13.class);
        dbMap.put(TEMPO_TEMPHUMI22,        BMTempHumiDatabase22.class);
        dbMap.put(TEMPO_TEMPHUMI23,        BMTempHumiDatabase23.class);
        dbMap.put(PEBBLE27,                BMPebbleDatabase27.class);
        dbMap.put(MOVE_LOGGER,             BMMoveLoggerDatabase32.class);
        dbMap.put(BM_BUTTON,               BMButtonDatabase42.class);
    }

    public final void clear(){
        for(String address : databaseMap.keySet()){
            BMDatabase database = databaseMap.get(address);
            database.close();
        }
    }

    /**
     * Creates a Blue Maestro device. If not found, will default to a generic Blue Maestro device.
     * @param id The ID of the Blue Maestro device
     * @param device The BluetoothDevice currently viewed
     * @return The Blue Maestro device
     */
    public final BMDevice createBMDevice(byte id, BluetoothDevice device){
        BMDevice bmDevice = null;
        Class<? extends BMDevice> clazz = dvMap.get(id);
        if(clazz != null){
            try {
                bmDevice = clazz
                            .getDeclaredConstructor(BluetoothDevice.class, byte.class)
                            .newInstance(device, id);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        if(bmDevice == null) bmDevice = new BMDefaultDevice(device, (byte) 0xFF);
        deviceMap.put(device.getAddress(), bmDevice);
        return bmDevice;
    }
    /**
     * Gets a current Blue Maestro device
     * @param address The address of the device
     * @return The Blue Maestro device
     */
    public final BMDevice getBMDevice(String address){
        return deviceMap.get(address);
    }

    /**
     * Creates a writable Blue Maestro database. If not found, returns null.
     * @param context The application context
     * @param bmDevice The Blue Maestro device
     * @return
     */
    public final BMDatabase createWritableBMDatabase(Context context, BMDevice bmDevice) {
        if(bmDevice == null) return null;
        Log.d("BMDeviceMap", "Device: " + bmDevice.getAddress() +
                " | Class: " + bmDevice.getClass().getSimpleName());
        BMDatabase bmDatabase = null;
        Class<? extends BMDatabase> clazz = dbMap.get(bmDevice.getVersion());
        if(clazz != null){
            try {
                bmDatabase = clazz
                                .getDeclaredConstructor(Context.class, bmDevice.getClass())
                                .newInstance(context, bmDevice);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        if(bmDatabase != null) databaseMap.put(bmDevice.getAddress(), bmDatabase);
        return bmDatabase;
    }

    /**
     * Gets a Blue Maestro database
     * @param address
     * @return
     */
    public final BMDatabase getBMDatabase(String address){
        return databaseMap.get(address);
    }

    /**
     * Creates a readable Blue Maestro database. If not found, returns null.
     * This should only be used for reading e.g. when looking at history. Due to the lack of
     * Blue Maestro device, it cannot write any new data.
     * @param context
     * @param address
     * @return
     */
    public final BMDatabase createReadableBMDatabase(Context context, String address){
        // If no database associated with this address, return no database
        if(!Arrays.asList(context.getApplicationContext().databaseList()).contains(address)){
            return null;
        }
        // Get generic database
        BMDatabase bmDatabase = new BMDefaultDatabase(context, address);
        // Get device version
        byte version = bmDatabase.getVersion();
        //String name = bmDatabase.getName();
        return bmDatabase;
        // Get specific database
        /*
        Class<? extends BMDatabase> clazz = dbMap.get(version);
        if(clazz != null){
            try {
                bmDatabase = clazz
                                .getDeclaredConstructor(Context.class, String.class)
                                .newInstance(context, address);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return bmDatabase;
        */
    }
}
