package com.bluemaestro.tempo_utility.views.graphs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Garrett on 11/08/2016.
 *
 * A Blue Maestro line chart. Includes easy setting of the legend, easy adding of entries,
 * and options such as threshold lines and zones.
 */
public class BMLineChart extends LineChart {

    private LineData data;
    private Map<String, Integer> dataSets;
    private Typeface font;

    private int width;
    private int height;

    private int xRange = 10;

    private List<LimitZone> zones;

    public BMLineChart(Context context) {
        super(context);
        this.data = new LineData();
        this.dataSets = new LinkedHashMap<String, Integer>();
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
        this.zones = new ArrayList<LimitZone>();
    }

    public BMLineChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.data = new LineData();
        this.dataSets = new LinkedHashMap<String, Integer>();
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
        this.zones = new ArrayList<LimitZone>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for(LimitZone zone : zones) zone.onDraw(canvas);
        super.onDraw(canvas);
    }

    /**
     * Initialises the chart
     * @param description The chart description
     * @param xRange The range of x values to display (unused)
     */
    public void init(String description, int xRange) {
        this.dataSets = new LinkedHashMap<String, Integer>();
        this.width = 900;
        this.height = 900;

        this.xRange = (xRange - 1 > 0) ? xRange - 1 : 0;
        int length = (width < height) ? width : height;

        setDescription(description);
        setDescriptionTypeface(font);
        setDescriptionTextSize(length * 10.0f / 800);

        //animateY(300);
        setNoDataText("No data available");

        YAxis leftAxis = getAxisLeft();
        leftAxis.setTypeface(font);
        leftAxis.setTextSize(length * 10.0f / 800);

        getAxisRight().setEnabled(false);

        XAxis xAxis = getXAxis();
        xAxis.setTypeface(font);
        xAxis.setTextSize(length * 10.0f / 800);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

    }

    /**
     * Sets the labels of the chart
     * @param labels The data labels
     * @param colors The colors for each label
     */
    public void setLabels(String[] labels, int[] colors) {
        List<ILineDataSet> dataLists = new ArrayList<ILineDataSet>();
        dataSets.clear();
        int length = (width < height) ? width : height;
        for(int index = 0; index < labels.length && index < colors.length; index++) {
            List<Entry> entries = new ArrayList<Entry>();
            LineDataSet dataSet = new LineDataSet(entries, labels[index]);
            dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSet.setColor(colors[index]);
            dataSet.setCircleColor(colors[index]);
            dataSet.setLineWidth(length * 2.0f / 800);
            dataSet.setCircleRadius(length * 3.0f / 800);
            dataSet.setCircleHoleRadius(length * 1.0f / 800);
            dataSet.setValueTypeface(font);
            dataSets.put(labels[index], index);
            dataLists.add(dataSet);
        }
        this.data = new LineData(dataLists);
        data.setValueTextSize(length * 7.0f / 800);
        setData(data);

        notifyDataSetChanged();

        Legend legend = getLegend();
        legend.setForm(Legend.LegendForm.SQUARE);
        legend.setTextSize(length * 10.0f / 800);
        legend.setTypeface(font);
        legend.setEnabled(true);
        legend.setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
        legend.setXEntrySpace(4.0f);

        invalidate();
    }

    /**
     * Adds an entry to the chart
     * @param label The data label to add the entry
     * @param entry The entry to add
     */
    public void addEntry(String label, Entry entry) {
        int index = dataSets.get(label);
        data.addEntry(entry, index);
        update();
    }

    /**
     * Gets an entry from the chart
     * @param label The data label the entry is under
     * @param index The index of the data value
     * @return The entry
     */
    public Entry getEntry(String label, int index){
        ILineDataSet dataSet = data.getDataSetByIndex(dataSets.get(label));
        return dataSet.getEntryForIndex(index);
    }

    /**
     * Gets the total number of entries
     * E.g. 3 data * 20 values = return 60 entries
     * @return
     */
    public int getEntryCount() {
        return data.getEntryCount();
    }

    /**
     * Adds a threshold line
     * @param threshold The threshold value
     * @param label The label for the line
     * @param color The color of the line (e.g. Color.argb(0x20, 0xCC, 0xCC, 0xCC))
     */
    public void addThresholdLine(float threshold, String label, int color){
        YAxis leftAxis = getAxisLeft();
        LimitLine ll = new LimitLine(threshold);
        ll.setLineColor(color);
        ll.setLineWidth(1f);
        ll.enableDashedLine(20f, 15f, 1f);
        ll.setLabel(label);
        ll.setTextSize(10f);
        ll.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        leftAxis.addLimitLine(ll);
    }

    /**
     * Adds a threshold zone
     * @param threshold The threshold value
     * @param op The operator to define the zone ("==", "<", ">", "<=", ">=")
     * @param label The label for the zone
     * @param color The color of the zone (e.g. Color.argb(0x20, 0xCC, 0xCC, 0xCC))
     */
    public void addThresholdZone(float threshold, String op, String label, int color){
        LimitZone zone = new LimitZone(threshold, op);
        zone.setViewPortHandler(mViewPortHandler);
        zone.setAxisTransformer(mLeftAxisTransformer);
        zone.setColor(color);
        zones.add(zone);

        YAxis leftAxis = getAxisLeft();
        LimitLine ll = new LimitLine(threshold);
        ll.setLineColor(color);
        ll.setLineWidth(1f);
        if(!op.equals("<=") && !op.equals(">=")){
            ll.enableDashedLine(20f, 15f, 1f);
        }
        ll.setLabel(label);
        ll.setTextSize(10f);
        if(op.equals(">") || op.equals(">=")){
            ll.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
        } else{
            ll.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        }
        leftAxis.addLimitLine(ll);
    }

    /**
     * Updates the chart
     */
    public void update() {
        notifyDataSetChanged();

        setVisibleXRangeMaximum(200);
        if(getVisibleXRange() >= 200){
            int keysetSize = dataSets.keySet().size();
            int lastPoint = getEntryCount() / keysetSize;
            moveViewToX(lastPoint - 200);
        }
        invalidate();
    }

    public File export(String filename) throws IOException, DocumentException {
        if(getWidth() <= 0 || getHeight() <= 0) return new File(filename);
        Bitmap bitmap = Bitmap.createBitmap(
                getWidth(),
                getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable drawable = getBackground();
        if(drawable != null){
            // Background available; draw background to canvas
            drawable.draw(canvas);
        }
        else{
            // Background not available; draw blank canvas
            canvas.drawColor(Color.WHITE);
        }
        // Draw view to canvas
        draw(canvas);

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(filename));
        document.open();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());

        Rectangle documentRect = document.getPageSize();
        image.scaleToFit(documentRect.getWidth(), documentRect.getHeight());
        image.scalePercent(50.0f);

        document.add(image);
        document.close();
        bitmap.recycle();

        return new File(filename);
    }
}
